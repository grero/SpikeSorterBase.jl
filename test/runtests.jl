using SpikeSorterBase
using Random
using Statistics
using StatsBase
using MAT
using Test

tp = [680.0139276241244,
      1462.2856921312978,
      3915.9422310941586,
      4720.074906453343,
      5139.54271773037]

@testset "Surrogates" begin
    RNG = MersenneTwister(1234)
    spikes, trialidx, λ = SpikeSorterBase.generate_spikes(tp, t0=-300.0, tf=100.0, τ1=25.0, τ2=15.0, RNG=RNG)
    @test length(spikes) == 631
    trial_counts = StatsBase.countmap(trialidx)
    @test trial_counts[1] == 120
    @test trial_counts[2] == 126
    @test trial_counts[3] == 125
    @test trial_counts[4] == 125
    @test trial_counts[5] == 135
end

@testset "Basic" begin
	temp1 = SpikeSorterBase.create_spike_template(60,3.0, 0.8, 0.2)
	@test temp1[1] ≈ 0.0
	@test temp1[end] ≈ 9.511124804405945e-17
	@test temp1[20] ≈ 0.1929695612885993 
	temp2 = SpikeSorterBase.create_spike_template(60,4.0, 0.3, 0.2)
	@test temp2[1] ≈ 0.0
	@test temp2[end] ≈ 1.0971647046208083e-18
	@test temp2[20] ≈ 0.35958849136683

	temps = cat(temp1, temp2, dims=2)
	ee = SpikeSorterBase.get_energy(temps,0.3) 
	@test ee[1] ≈ 30.21562696874068
	@test ee[2] ≈ 50.48392884173489
	RNG = MersenneTwister(1234)
	S = SpikeSorterBase.create_signal(20_000, 0.3, [0.003, 0.001], temps;rng=RNG)
	Q = MAT.matread("signal1.mat")
	@test S ≈ Q["data"]
end

@testset "Energy" begin
	Q = MAT.matread("signal1.mat")
	RNG = MersenneTwister(1234)
	σ² = var(Q["data"])
	ee = SpikeSorterBase.get_noise_energy(Q["data"], 1/σ², 60, rng=RNG)
	@test ee ≈ 12.319147532816185 
end

@testset "Full signal" begin
    RNG = MersenneTwister(1234)
    S, spikes = SpikeSorterBase.create_signal(tp, RNG=RNG)
    @test length(S) == 156406 
    @test std(S) ≈ 1.1306829877048794
    @test length(spikes[1]) == 631
    @test length(spikes[2]) == 286
end
