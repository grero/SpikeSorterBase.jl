using Coverage
cd(joinpath(@__DIR__, "..", "..")) do
	covered_lines, total_lines = get_summary(process_folder())
	percentage = 100*covered_lines/total_lines
	println("($(percentage)%) covered")
end
