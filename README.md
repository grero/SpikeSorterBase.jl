# SpikeSorterBase

A base package for various spike sorting tools. The main purpose of the package is to provide benchmark examples against which spike sorter algorithms can be tested.