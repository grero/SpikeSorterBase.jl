function check_spike!(spikes, q, r, t;RNG=MersenneTwister(rand(UInt32)))
    if q >= r
        push!(spikes, t)
        r = -log(rand(RNG))
        q = 0.0
    end
    return q,r
end

fr(x) = 0.1*(0.05 .+ sum(exp.(-x.^2/(25.0^2))))

function generate_spikes(timestamps::Vector{Float64};τ1=75.0, τ2=25.0,
                                                     t0=-400.0, tf=100.0,
                                                     λ0=0.0001,RNG=MersenneTwister(rand(UInt32)))
    spikes = Float64[]
    trialidx = Int64[]
    λ = fill(0.0, 501,length(timestamps))
    q = 0.0
    r = -log(rand(RNG))
    i = 1
    t = 0.0
    t1 = 0.0
    dt = 1.0
    done = false
    for (ii,ts) in enumerate(timestamps)
        t = t0 
        _λ = λ0
        q = 0.0
        r = -log(rand(RNG))
        jj = 1
        while t < 0.0 
            _λ += _λ*dt/τ1 
            λ[jj,ii] = _λ
            q += _λ*dt
            q,r = check_spike!(spikes, q,r,t+ts,RNG=RNG)
            if q == 0.0
                push!(trialidx, ii)
            end
            t += dt
            jj += 1
        end
        #draining phase
        while t < tf 
            _λ -= _λ*dt/τ2
            _λ = max(0.0, _λ)
            λ[jj,ii] = _λ
            q += _λ*dt
            q,r = check_spike!(spikes, q,r,t+ts, RNG=RNG)
            if q == 0.0
                push!(trialidx, ii)
            end
            t += dt
            jj += 1
        end
    end
    spikes, trialidx, λ
end
