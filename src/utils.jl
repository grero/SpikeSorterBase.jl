create_spike_template(nstates::Integer) = create_template(nstates, 1.0, 0.8, 0.2)

function create_spike_template(nstates::Integer, a::Real, b::Real, c::Real)
    x = range(0,stop=1.5,length=nstates)
    y = a*sin.(2*pi*x).*exp.(-(b .- x).^2/c)
    return y
end

function create_signal(N::Integer, sigma::Number, pp::Array{Float64,1}, templates::Array{Float64,2};rng=MersenneTwister(rand(UInt32)))
    nstates,ncells = size(templates)
    state = zeros(Int64, ncells)
    S = sigma.*randn(rng, N) 
    active_cell = 0
    for i in 1:length(S)
        if active_cell == 0
            for j in 1:ncells
                r = rand(rng)
                if pp[j] > r
                    state[j] = 1
                    active_cell = j
                    break
                else
                    active_cell = 0
                    state[j] = 0
                end
            end
        end
        if active_cell > 0
            S[i] += templates[state[active_cell], active_cell]
            state[active_cell] += 1
            if state[active_cell] > nstates
                state[active_cell] = 0
                active_cell = 0
            end
        end
    end
    return S
end

function create_signal(timestamps::Vector{Vector{Int64}}, templates::Vector{Vector{Float64}}, σ::Real;RNG=MersenneTwister(rand(UInt32)))
    tmin = minimum(minimum.(timestamps))
    tmax = maximum(maximum.(timestamps))
    nstates = maximum(length.(templates))
    signal_length = tmax+nstates
    S = σ*randn(RNG, signal_length)
    for (_timestamps, template) in zip(timestamps, templates)
        trigger = argmin(template)
        nn = length(template)
        for _t in _timestamps
            S[(_t-trigger+1):(_t + (nn-trigger))] .+= template
        end
    end
    return S
end

function create_signal(tp::Vector{T};RNG=MersenneTwister(rand(UInt32)),sampling_rate=30_000.0) where T <: Real
	temp1 = create_spike_template(60,3.0, 0.8, 0.2)
	temp2 = create_spike_template(60,4.0, 0.3, 0.2)
    fs = sampling_rate/1000.0
    temps = [temp1, temp2]
    RNG = MersenneTwister(1234)
    spikes1, trialidx1, λ1 = generate_spikes(tp, t0=-300.0, tf=100.0, τ1=25.0, τ2=15.0, RNG=RNG)
    spikes2, trialidx2, λ2 = generate_spikes(tp, t0=-300.0, tf=100.0, τ1=30.0, τ2=15.0, RNG=RNG)
    spikes = [round.(Int64, fs*_spikes) for _spikes in [spikes1, spikes2]]
    S = create_signal(spikes, temps, 0.5, RNG=RNG)
    return S, spikes
end

function get_chunk(X::AbstractArray{Float64,1},idx,chunksize=100_000)
    X[(idx-1)*chunksize+1:idx*chunksize]
end

"""
Estimate the noise energy of data by computing the normalized energy in `nsamples` random patches of length `nstates`.
"""
function get_noise_energy(data::AbstractVector{T1}, cinv::T2, nstates::Int64, nsamples=1000;rng=MersenneTwister(rand(UInt32))) where T1 <: Real where T2 <: Real
    N = length(data)
    samples = zeros(nsamples)
    idx = rand(rng, 1:N-nstates, nsamples)
    sort!(idx)
    for i in 1:nsamples
        @inbounds ii = idx[i]
        s = 0.0
        for j in 1:nstates
            x = data[ii+j-1]
            s += x*cinv*x
        end
        @inbounds samples[i] = s
    end
    return median(samples)
end

function get_energy(waveforms::Array{Float64,2}, cinv::Float64)
    nstates, N = size(waveforms)
    ee = zeros(N)
    for i in 1:N
        _ee = 0.0
        for j in 1:nstates
            @inbounds w = waveforms[j,i]
            _ee += w*cinv*w
        end
        @inbounds ee[i] = _ee
    end
    ee
end
